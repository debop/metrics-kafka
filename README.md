# metrics-kafka 

Application metrics 정보를 kafka 를 이용하여 수집할 수 있도록 하는 reporter 및 consumer 를 제공합니다. 

## Features

## Usage

### gradle

```groovy
compile "debop4k:metrics-kafka:0.1.0"
```

### maven

```xml
<dependency>
    <groupId>debop4k</groupId>
    <artifactId>metrics-kafka</artifactId>
    <version>0.1.0</version>
</dependency>
```

## Examples