package com.coupang.metrics.kafka.reporters

import com.codahale.metrics.*
import com.codahale.metrics.Timer
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.ProducerRecord
import org.nustaq.serialization.FSTConfiguration
import org.slf4j.LoggerFactory
import java.util.*
import java.util.concurrent.*

/**
 * Application Metrics 정보를 주기적으로 Kafka 를 통해 전송하는 Reporter 입니다.
 *
 * @author sunghyouk.bae@gmail.com
 * @since  2017-12-03
 */
class KafkaReporter(val registry: MetricRegistry,
                    val name: String,
                    val filter: MetricFilter,
                    val rateUnit: TimeUnit,
                    val durationUnit: TimeUnit,
                    val producer: KafkaProducer<String, ByteArray>,
                    val topic: String = "test.report.t",
                    val showSamples: Boolean = true,
                    val prefix: String = "test.",
                    val hostName: String = "localhost",
                    val ip: String = "127.0.0.1")
    : ScheduledReporter(registry, name, filter, rateUnit, durationUnit) {

    companion object {
        private val log = LoggerFactory.getLogger(KafkaReporter::class.java)
        private val fst = FSTConfiguration.getDefaultConfiguration()
    }

    private var count = 0L

    override fun report(gauges: SortedMap<String, Gauge<Any>>,
                        counters: SortedMap<String, Counter>,
                        histograms: SortedMap<String, Histogram>,
                        meters: SortedMap<String, Meter>,
                        timers: SortedMap<String, Timer>) {

        try {
            val metric = hashMapOf<String, Any>("hostName" to hostName,
                                                "ip" to ip,
                                                "rateUnit" to rateUnit,
                                                "durationUnit" to durationUnit,
                                                "gauges" to addPrefix(gauges),
                                                "counters" to addPrefix(counters),
                                                "histograms" to addPrefix(histograms),
                                                "meters" to addPrefix(meters),
                                                "timers" to addPrefix(timers))

            val value = fst.asByteArray(metric)
            val record = ProducerRecord<String, ByteArray>(topic, "${count++}", value)
            producer.send(record)
        } catch (ignored: Exception) {
            log.error("Fail to send metrics to kafka.", ignored)
        }
    }

    private fun addPrefix(map: SortedMap<String, *>): Map<String, Any> {
        return map.map { entry ->
            Pair(prefix + entry.key, entry.value)
        }.toMap()
    }
}