package com.coupang.metrics.kafka.consumers

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.nustaq.serialization.FSTConfiguration
import org.slf4j.LoggerFactory
import java.io.Closeable
import java.util.concurrent.atomic.*

abstract class AbstractMetricsKafkaConsumer(val consumer: KafkaConsumer<String, ByteArray>) : Closeable {

    companion object {
        private val log = LoggerFactory.getLogger(AbstractMetricsKafkaConsumer::class.java)

        private val fst = FSTConfiguration.getDefaultConfiguration()
    }

    private val canConsume = AtomicBoolean(true)
    private lateinit var job: Deferred<Unit>

    abstract suspend fun handleMetric(metrics: HashMap<String, Any>)

    /**
     * Aggregate Metrics informations
     */
    open fun consume() {
        if (!canConsume.get())
            return

        job = async(CommonPool) {
            while (canConsume.get()) {
                val records = async { consumer.poll(100) }.await()

                records.forEach { record ->
                    try {
                        val bytes = record.value()
                        val metrics = fst.asObject(bytes) as? HashMap<String, Any>
                        metrics?.let {
                            launch(coroutineContext) {
                                handleMetric(it)
                            }
                        }
                    } catch (ignored: Exception) {
                        log.error("Fail to received value.", ignored)
                    }
                }
            }
        }
    }

    /**
     * Stop consuming
     */
    open fun stop(): Boolean {
        if (canConsume.compareAndSet(true, false)) {
            job.cancel()
            return true
        }
        return false
    }

    open fun resume(): Boolean {
        if (canConsume.compareAndSet(false, true)) {
            consume()
            return true
        }
        return false
    }

    override fun close() {
        stop()
        Thread.sleep(100)
        consumer.close()
    }
}