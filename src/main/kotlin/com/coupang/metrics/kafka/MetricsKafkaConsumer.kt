package com.coupang.metrics.kafka

import org.slf4j.LoggerFactory

/**
 * Metrics Reporter 가 Kafka를 통해 발송한 metrics 정보를 받아옵니다.
 */
class MetricsKafkaConsumer {

    companion object {
        private val log = LoggerFactory.getLogger(MetricsKafkaConsumer::class.java)
    }
}