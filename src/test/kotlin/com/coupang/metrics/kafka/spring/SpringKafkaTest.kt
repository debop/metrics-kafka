package com.coupang.metrics.kafka.spring

import com.codahale.metrics.MetricRegistry
import com.codahale.metrics.jvm.FileDescriptorRatioGauge
import com.codahale.metrics.jvm.GarbageCollectorMetricSet
import com.codahale.metrics.jvm.MemoryUsageGaugeSet
import com.codahale.metrics.jvm.ThreadStatesGaugeSet
import com.coupang.metrics.kafka.reporters.KafkaReporter
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.test.context.junit4.SpringRunner
import java.util.concurrent.*
import javax.annotation.PostConstruct
import kotlin.concurrent.thread

@RunWith(SpringRunner::class)
@SpringBootTest(classes = [SpringKafkaTest.TestConfig::class])
class SpringKafkaTest {

    @Configuration
    open class TestConfig {

        @Bean
        open fun annotationObject(): AnnotationObject {
            return AnnotationObject()
        }

        @Bean
        open fun metricRegistry(): MetricRegistry {
            return MetricRegistry()
        }

        @PostConstruct
        open fun setup() {
            val registry = metricRegistry()
            registry.register("jvm.gc", GarbageCollectorMetricSet())
            registry.register("jvm.memory", MemoryUsageGaugeSet())
            registry.register("jvmthread-states", ThreadStatesGaugeSet())
            registry.register("jvm.fd.usage", FileDescriptorRatioGauge())
        }
    }


    companion object {

        private val log = org.slf4j.LoggerFactory.getLogger(SpringKafkaTest::class.java)

        // Embedded Kafka
        // TODO Spring-Kafka의 Embedded Kafka 정의

    }

    @Autowired lateinit var annotationObject: AnnotationObject
    @Autowired lateinit var metrics: MetricRegistry

    @Before
    fun setup() {
        startKafkaReport()
    }

    private fun startKafkaReport() {
        log.debug("Start Kafka reporter...")

        val kafkaReporter = KafkaReporter(metrics)
        kafkaReporter.start(5, TimeUnit.SECONDS)
    }

    @Test
    fun `run annotated object for report`() {
        thread(start = true) {
            while (true) {
                annotationObject.call()
                annotationObject.userLogin()
            }
        }

        Thread.sleep(500_000)
    }
}