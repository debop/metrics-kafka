package com.coupang.metrics.kafka.spring

import com.codahale.metrics.annotation.Counted
import com.codahale.metrics.annotation.Timed
import java.util.*


open class AnnotationObject {

    companion object {
        val random = Random(System.currentTimeMillis())
    }

    @Timed
    open fun call() {
        Thread.sleep(random.nextInt(3000).toLong())
    }

    @Counted
    open fun userLogin() {
        // Nothing to do 
    }
}