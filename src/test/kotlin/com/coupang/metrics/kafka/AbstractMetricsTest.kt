package com.coupang.metrics.kafka

import org.slf4j.LoggerFactory

abstract class AbstractMetricsTest {

    companion object {
        val log = LoggerFactory.getLogger(AbstractMetricsTest::class.java.name)
    }
}